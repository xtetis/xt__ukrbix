/**
 * Автоматическая настройка popover
 */
//=====================================================================
function setTooltips() {


    $(".tooltips_data > div").each(function (index) {
        var idx = $(this).attr('idx');
        var title = $(this).find('.ttitle').html();
        var content = $(this).find('.tcontent').html();

        $('#' + idx).attr('data-toggle', 'popover');
        $('#' + idx).attr('data-content', content);
        $('#' + idx).attr('title', title);
        console.log(idx);
    });

    $('[data-toggle="popover"]').popover({
        trigger: 'focus hover',
        placement: 'top',
        html: true
    });
    console.log('popover');
}
//=====================================================================


$(document).ready(function () {
    setTooltips();

    $(document).ajaxComplete(function () {
        setTooltips();
    });
});