<?php

namespace app\modules\board\controllers;

use app\models\Board;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class ItemController extends Controller
{

    /**
     * @return mixed
     */
    public function actionIndex($id) 
    {
        $model = $this->findModel($id);

        return $this->render('index', [
            'model'       => $model,
        ]);

    }

    /**
     * @param $id
     * @return mixed
     */
    protected function findModel($id)
    {
        if (($model = Board::findOne($id)) !== null)
        {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
