<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;


    $this->title                   = $model->name;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Объявления'), 'url' => ['/board']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="board-create">



    <div class="board-item row">
        <div class="col">
            <h1><?=Html::encode($this->title)?></h1>
            <div class="">Местоположение</div>
            <div class="">Добавлено: <?=$model->create_date?>, номер объявления <?=$model->id?></div>
            <div class="">Слайдер с картинками</div>
            <div class="">Параметры объявления</div>
            <div class="card">
                <div class="card-body">
                    <?=$model->about?>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    Похожие объявления
                </div>
                <div class="card-body">
                    zzzzzzzzzzzzzzzzza
                </div>
            </div>
        </div>
        <div class="col-3">
            <?php if (intval($model->price)): ?>
            <div class="board_item_price"><?=$model->price?> <?=$model->id_currency?></div>
            <?php endif; ?>
            <div>Связаться с автором Вадим</div>
            <div>Написать автору</div>
            <div>Телефон</div>
            <div>Местоположение</div>
            <div>Пожаловаться</div>
            <div>Social icons</div>

        </div>





    </div>
</div>
