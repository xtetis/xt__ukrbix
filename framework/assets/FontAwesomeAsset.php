<?php

namespace app\assets;

use yii\web\AssetBundle;


class FontAwesomeAsset extends AssetBundle
{
    //public $basePath = '@webroot';
    //public $baseUrl = '@web';
    public $sourcePath = '@vendor/components/font-awesome/';
    public $css = [
        'css/fontawesome-all.min.css',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

}