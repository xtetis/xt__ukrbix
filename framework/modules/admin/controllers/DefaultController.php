<?php

namespace app\modules\admin\controllers;

use yii\web\Controller;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{

    public function init()
    {
        parent::init();
        
        /**
         * Указываем сайдбар для модуля - меню другое
         */
        //\Yii::setAlias('@sidebar_path', '@sidebar_path_admin');
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }


    public function actionMyRestAction() {
        //return \Yii::$app()->runAction("sample/my-action");
    }
}
