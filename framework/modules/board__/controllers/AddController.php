<?php

namespace app\modules\board\controllers;

use Yii;
use app\models\City;
use app\models\Board;
use app\models\Region;
use yii\web\Controller;
use app\models\District;
use app\models\BoardCategory;
use dektrium\user\models\User;
use xtetis\image\models\Album;

class AddController extends Controller
{

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        
        $board_add['id_city'] = intval(\Yii::$app->request->get('id_city', 0));

        $modelAlbum               = new Album();
        $modelAlbum->images_limit = 7;

        $modelBoard          = new Board();
        $modelBoard->id_user = 0;

        $modelAlbum->load(\Yii::$app->request->post());


        $validatedUser = (!Yii::$app->user->isGuest);
        $modelUser = false;
        if (!$validatedUser)
        {
            $modelUser = Yii::createObject([
                'class'    => User::className(),
                'scenario' => 'create',
            ]);
            $validatedUser = (
                ($modelUser->load(\Yii::$app->request->post())) &&
                ($modelUser->validate())
            );
        }

        if (
            ($modelBoard->load(\Yii::$app->request->post())) &&
            ($modelBoard->validate()) &&
            ($validatedUser) &&
            ($modelAlbum->validate())
        )
        {
            if (Yii::$app->user->isGuest)
            {
                $modelUser->create();
                $modelBoard->id_user = $modelUser->id;
            }
            else
            {
                $modelBoard->id_user = Yii::$app->user->id;
            }

            $modelBoard->id_album = intval($modelAlbum->addAlbum());
            $modelBoard->save();

            // Если пользователь только что создан
            if ($modelUser)
            {
                /**
                 * Проставляем в сессию сгенерированный пароль, мообщение, почту и т.д.
                 */
                $session = Yii::$app->session;
                $attributes = $modelUser->getAttributes();
                $attributes['password'] = $modelUser->password;
                $session['user'] = $attributes;
                $session['board_item'] = $modelBoard->id;
                
                // Авторизируемся
                Yii::$app->user->switchIdentity($modelUser);
                return $this->redirect(['/board/add/newuser']);
            }
            else
            {
                return $this->redirect(['/board/item/' . $modelBoard->id]);
            }
            
        }

        if ($modelBoard->id_city)
        {
            $board_add['id_city'] = $modelBoard->id_city;
        }

        $selected_id_city = intval($board_add['id_city']);
        $districts        = District::find()->where(['id_city' => $selected_id_city])->all();

        // Если установлены город или категория - формируем текстовое значение
        //----------------------------------------------------
        $name_id_city = '';
        if ($modelBoard->id_city)
        {
            $modelCity = City::find()->where(['id' => $modelBoard->id_city])->one();
            if ($modelCity)
            {
                $modelRegion = $modelCity->getRegion()->one();
                if ($modelRegion)
                {
                    $name_id_city = $modelRegion->name . ' -> ' . $modelCity->name;
                }
            }
        }

        $name_id_board_category = '';
        if ($modelBoard->id_board_category)
        {
            $modelBoardCategory = BoardCategory::find()->where(['id' => $modelBoard->id_board_category])->one();
            if ($modelBoardCategory)
            {
                $modelBoardCategoryParent = BoardCategory::find()->where(['id' => $modelBoardCategory->id_parent])->one();
                if ($modelBoardCategoryParent)
                {
                    $name_id_board_category = $modelBoardCategoryParent->name . ' -> ' . $modelBoardCategory->name;
                }
            }
        }
        //----------------------------------------------------

        $regions          = Region::find()->all();
        $cities           = City::find()->all();
        $board_categories = BoardCategory::find()->all();

        return $this->render('index', [
            'model'                  => $modelBoard,
            'modelAlbum'             => $modelAlbum,
            'board_add'              => $board_add,
            'districts'              => $districts,
            'name_id_city'           => $name_id_city,
            'name_id_board_category' => $name_id_board_category,
            'regions'                => $regions,
            'cities'                 => $cities,
            'board_categories'       => $board_categories,
            'modelUser'              => $modelUser,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionNewuser()
    {
        $session = Yii::$app->session;

        return $this->render('newuser', [
           'user' => $session['user'],
           'board_item' => $session['board_item'],
        ]);
    }
    
}
