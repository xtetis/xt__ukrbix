# xt__ukrbix



composer require --prefer-dist yii2mod/yii2-rbac "*"
composer require --prefer-dist yii2mod/yii2-settings "*"
composer require dektrium/yii2-user




Миграции: применение

php yii migrate/up
php yii migrate/down


1.Миграция модуля пользователи

    //php yii migrate/up --migrationPath=@vendor/dektrium/yii2-user/migrations


2. Миграции моих таблиц
    ./yii migrate/create create_region_table --fields="id:primaryKey,name:string(200),active:integer:defaultValue(1)"
    ./yii migrate/create create_city_table --fields="id:primaryKey,id_region:integer:notNull:foreignKey(region),name:string(200),active:integer:defaultValue(1)"
    ./yii migrate/create create_district_table --fields="id:primaryKey,id_city:integer:notNull:foreignKey(city),name:string(200),active:integer:defaultValue(1)"


3. Миграции модуля https://github.com/phemellc/yii2-settings

    ./yii migrate/up --migrationPath=@vendor/pheme/yii2-settings/migrations

3. Миграции модуля yii2mod/yii2-rbac

    php yii migrate/up --migrationPath=@yii/rbac/migrations

4. Миграция модуля dvizh/yii2-gallery

    php yii migrate/up --migrationPath=@vendor/dvizh/yii2-gallery/src/migrations
