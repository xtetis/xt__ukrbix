<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%phone}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 */
class m190404_191547_create_phone_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%phone}}', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer()->notNull(),
            'phone' => $this->string(20)->append('CHARACTER SET utf8 COLLATE utf8_general_ci'),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        // creates index for column `id_user`
        $this->createIndex(
            '{{%idx-phone-id_user}}',
            '{{%phone}}',
            'id_user'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-phone-id_user}}',
            '{{%phone}}',
            'id_user',
            '{{%user}}',
            'id',
            'CASCADE'
        );


        $this->addCommentOnColumn('{{%phone}}','id', 'Первичный ключ');
        $this->addCommentOnColumn('{{%phone}}','id_user', 'Ссылка на пользователя');
        $this->addCommentOnColumn('{{%phone}}','phone', 'Номер телефона в цифрах с кодом страны');

        $this->addCommentOnTable('{{%phone}}','Список телефонов пользователей');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-phone-id_user}}',
            '{{%phone}}'
        );

        // drops index for column `id_user`
        $this->dropIndex(
            '{{%idx-phone-id_user}}',
            '{{%phone}}'
        );

        $this->dropTable('{{%phone}}');
    }
}
