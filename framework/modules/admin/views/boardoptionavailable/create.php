<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BoardOptionAvailable */

$this->title = Yii::t('app', 'Create Board Option Available');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Board Option Availables'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="board-option-available-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
