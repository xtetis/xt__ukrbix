<VirtualHost 127.0.0.1:443>
    DocumentRoot "/var/www/ukrbix.lc/public_html"
    ServerName ukrbix.lc
    SSLEngine on
    SSLCertificateFile "/var/www/ukrbix.lc/framework/ssl/server.crt" 
    SSLCertificateKeyFile "/var/www/ukrbix.lc/framework/ssl/server.key"
    <Directory "/var/www/ukrbix.lc/public_html/">
		Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>


Create a Self-signed Certificate
1. In a directory of your choosing, create an RSA Private Key:

openssl genrsa -des3 -out server.key 1024
Copy
2. Create a certificate signing request (CSR):

openssl req -new -key server.key -out server.csr
Copy
3. Remove the passphrase. If you enable the passphrase, Apache will ask for the passphrase each time the web server is started:


cp server.key server.key.org
openssl rsa -in server.key.org -out server.key
Copy
4. Create the self-signed certificate:

openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt
Copy



http://www.zunisoft.com/2008/10/10/kb-xampp-ssl-configuration-on-ubuntu/

https://stackoverflow.com/questions/5801425/enabling-ssl-with-xampp