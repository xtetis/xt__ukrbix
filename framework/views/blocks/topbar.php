<nav class="navbar header-navbar pcoded-header">
    <div class="navbar-wrapper">

        <div class="navbar-logo">
            <a class="mobile-menu"
               id="mobile-collapse"
               href="#!">
                <i class="feather icon-menu"></i>
            </a>
            <a href="/">
                <img class="img-fluid"
                     src="/theme/templates/adminty/files/assets/images/logo.png"
                     alt="Theme-Logo">
            </a>
            <a class="mobile-options">
                <i class="feather icon-more-horizontal"></i>
            </a>
        </div>

        <div class="navbar-container container-fluid">
            <ul class="nav-left">
                <li>
                    <a href="#!"
                       onclick="javascript:toggleFullScreen()">
                        <i class="feather icon-maximize full-screen"></i>
                    </a>
                </li>
            </ul>
            <ul class="nav-right">
                <li class="header-notification">
                    <div class="dropdown-primary dropdown">
                        <div class="dropdown-toggle"
                             data-toggle="dropdown">
                            <i class="feather icon-bell"></i>
                            <span class="badge bg-c-pink">5</span>
                        </div>
                        <ul class="show-notification notification-view dropdown-menu"
                            data-dropdown-in="fadeIn"
                            data-dropdown-out="fadeOut">
                            <li>
                                <h6>Notifications</h6>
                                <label class="label label-danger">New</label>
                            </li>
                            <li>
                                <div class="media">
                                    <img class="d-flex align-self-center img-radius"
                                         src="/theme/templates/adminty/files/assets/images/avatar-4.jpg"
                                         alt="Generic placeholder image">
                                    <div class="media-body">
                                        <h5 class="notification-user">John Doe</h5>
                                        <p class="notification-msg">Lorem ipsum dolor sit amet,
                                            consectetuer elit.</p>
                                        <span class="notification-time">30 minutes ago</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="media">
                                    <img class="d-flex align-self-center img-radius"
                                         src="/theme/templates/adminty/files/assets/images/avatar-3.jpg"
                                         alt="Generic placeholder image">
                                    <div class="media-body">
                                        <h5 class="notification-user">Joseph William</h5>
                                        <p class="notification-msg">Lorem ipsum dolor sit amet,
                                            consectetuer elit.</p>
                                        <span class="notification-time">30 minutes ago</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="media">
                                    <img class="d-flex align-self-center img-radius"
                                         src="/theme/templates/adminty/files/assets/images/avatar-4.jpg"
                                         alt="Generic placeholder image">
                                    <div class="media-body">
                                        <h5 class="notification-user">Sara Soudein</h5>
                                        <p class="notification-msg">Lorem ipsum dolor sit amet,
                                            consectetuer elit.</p>
                                        <span class="notification-time">30 minutes ago</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="header-notification">
                    <div class="dropdown-primary dropdown">
                        <div class="displayChatbox dropdown-toggle"
                             data-toggle="dropdown">
                            <i class="feather icon-message-square"></i>
                            <span class="badge bg-c-green">3</span>
                        </div>
                    </div>
                </li>
                <li class="user-profile header-notification">
                    <div class="dropdown-primary dropdown">
                        <?php if (Yii::$app->user->isGuest): ?>
                        <div class="dropdown-toggle"
                             data-toggle="dropdown">
                            <img src="/theme/templates/adminty/files/assets/images/avatar-4.jpg"
                                 class="img-radius"
                                 alt="User-Profile-Image">
                            <i class="feather icon-chevron-down"></i>
                        </div>
                        <ul class="show-notification profile-notification dropdown-menu"
                            data-dropdown-in="fadeIn"
                            data-dropdown-out="fadeOut">
                            <li>
                                <a href="/user/security/login">
                                    <i class="feather icon-settings"></i> Авторизация
                                </a>
                            </li>
                            <li>
                                <a href="/user/registration/register">
                                    <i class="feather icon-user"></i> Регистрация
                                </a>
                            </li>
                        </ul>
                        <?php else: ?>
                        <div class="dropdown-toggle"
                             data-toggle="dropdown">
                            <img src="/theme/templates/adminty/files/assets/images/avatar-4.jpg"
                                 class="img-radius"
                                 alt="User-Profile-Image">
                            <span><?=\Yii::$app->user->identity->username?></span>
                            <i class="feather icon-chevron-down"></i>
                        </div>
                        <ul class="show-notification profile-notification dropdown-menu"
                            data-dropdown-in="fadeIn"
                            data-dropdown-out="fadeOut">
                            <li>
                                <a href="/user/settings/account">
                                    <i class="feather icon-settings"></i> Настройки
                                </a>
                            </li>
                            <li>
                                <a href="/user/settings/profile">
                                    <i class="feather icon-user"></i> Профиль
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="feather icon-mail"></i> Мои сообщения
                                </a>
                            </li>
                            <li>
                                <a href="/user/settings/networks">
                                    <i class="feather icon-facebook"></i> Соцсети
                                </a>
                            </li>
                            <?php if (isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())['admin'])): ?>
                            <li>
                                <a class="dropdown-item"
                                   href="/admin">
                                    <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Админка
                                </a>
                            </li>
                            <?php endif; ?>
                            <li>
                                <a href="#"
                                   data-toggle="modal"
                                   data-target="#logoutModal">
                                    <i class="feather icon-log-out"></i> Выход
                                </a>
                            </li>
                        </ul>
                        <?php endif; ?>



                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>





<!-- Logout Modal-->
<div class="modal fade"
     id="logoutModal"
     tabindex="-1"
     role="dialog"
     aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog"
         role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"
                    id="exampleModalLabel">Хотите выйти?</h5>
                <button class="close"
                        type="button"
                        data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Выберите «Выход» из списка ниже, если вы готовы завершить текущий сеанс.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary"
                        type="button"
                        data-dismiss="modal">Отмена</button>
                <a class="btn btn-primary"
                   data-method="post"
                   href="/user/security/logout">Выход</a>
            </div>
        </div>
    </div>
</div>
