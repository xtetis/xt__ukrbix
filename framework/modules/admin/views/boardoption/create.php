<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BoardOption */

$this->title = Yii::t('app', 'Create Board Option');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Board Options'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="board-option-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
