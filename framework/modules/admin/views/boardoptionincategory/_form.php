<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BoardOptionInCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="board-option-in-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_board_option')->textInput() ?>

    <?= $form->field($model, 'id_board_category')->textInput() ?>

    <?= $form->field($model, 'use_setting')->textInput() ?>

    <?= $form->field($model, 'use_filter')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
