<?php

    /**
     * @param $group_item
     */
    function isActiveSubitem($subitem)
    {
        $ret = false;
        if (
            (isset($subitem['ids']['module_id'])) &&
            (isset($subitem['ids']['controller_id'])) &&
            (isset($subitem['ids']['action_id']))
        )
        {

            if (
                (\Yii::getAlias('@module_id') == $subitem['ids']['module_id']) ||
                ('*' == $subitem['ids']['module_id'])
            )
            {
                if (
                    (\Yii::getAlias('@controller_id') == $subitem['ids']['controller_id']) ||
                    ('*' == $subitem['ids']['controller_id'])
                )
                {
                    if (
                        (\Yii::getAlias('@action_id') == $subitem['ids']['action_id']) ||
                        ('*' == $subitem['ids']['action_id'])
                    )
                    {
                        $ret = true;
                    }
                }
            }

        }

        return $ret;
    }

    /**
     * @param $group_item
     */
    function getSubitemTagOpts($subitem)
    {
        $ret = '';
        if (isset($subitem['tagopts']))
        {
            $ret = $subitem['tagopts'];
        }

        return $ret;
    }

    /**
     * @param $group_item
     */
    function isActiveGroup($group_item)
    {
        $ret = false;
        if ((isset($group_item['force-expand'])) && ($group_item['force-expand']))
        {
            return true;
        }

        if (isset($group_item['items']))
        {
            foreach ($group_item['items'] as $subitem)
            {
                if (isActiveSubitem($subitem))
                {
                    $ret = true;
                    break;
                }
            }
        }

        if (isset($group_item['ids']))
        {
            $ret = isActiveSubitem($group_item);
        }

        return $ret;
    }

    $sidebar['frontend'] = [
        [
            'group_items' => [
                [
                    'title' => 'Главная',
                    'icon'  => '<i class="feather icon-home"></i>',
                    'href'  => '/',
                    'ids'   => [
                        'module_id'     => 'basic',
                        'controller_id' => 'site',
                        'action_id'     => '*',
                    ],
                ],
            ],
        ],
        [
            //'group_title' => 'Объявления',
            'group_items' => [
                [
                    'title'  => 'Объявления',
                    'icon'   => '<i class="feather icon-menu"></i>',
                    'force-expand' => true,
                    'href'  => '/board',
                    'items'  => [
                        [
                            'href'  => '/board',
                            'title' => 'Все объявления',
                            'ids'   => [
                                'module_id'     => 'board',
                                'controller_id' => 'default',
                                'action_id'     => 'index',
                            ],
                        ],
                        [
                            'href'  => '/board/add',
                            'title' => 'Добавить объявления',
                            'ids'   => [
                                'module_id'     => 'board',
                                'controller_id' => 'add',
                                'action_id'     => '*',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ];

    $sidebar['backend'] = [
        [
            'group_title' => 'Админка',
            'group_items' => [
                [
                    'title' => 'Разное',
                    'icon'  => '<i class="feather icon-home"></i>',
                    'items' => [
                        [
                            'href'  => '/admin',
                            'title' => 'Админка',
                            'ids'   => [
                                'module_id'     => 'admin',
                                'controller_id' => 'default',
                                'action_id'     => '*',
                            ],
                        ],
                        [
                            'href'  => '/settings',
                            'title' => 'Настройки',
                            'ids'   => [
                                'module_id'     => 'settings',
                                'controller_id' => '*',
                                'action_id'     => '*',
                            ],
                        ],
                        [
                            'href'  => '/gallery',
                            'title' => 'Картинки',
                            'ids'   => [
                                'module_id'     => 'gallery',
                                'controller_id' => '*',
                                'action_id'     => '*',
                            ],
                        ],
                        [
                            'href'    => '/debug',
                            'title'   => 'Отладка',
                            'tagopts' => 'target="_blank"',
                        ],
                        [
                            'href'    => '/gii',
                            'title'   => 'Gii',
                            'tagopts' => 'target="_blank"',
                        ],
                    ],
                ],
                [
                    'title' => 'Справочники',
                    'icon'  => '<i class="feather icon-menu"></i>',
                    'items' => [
                        [
                            'href'  => '/admin/boardcategory',
                            'title' => 'Категории объявлений',
                            'ids'   => [
                                'module_id'     => 'admin',
                                'controller_id' => 'boardcategory',
                                'action_id'     => '*',
                            ],
                        ],
                        [
                            'href'  => '/admin/boardoption',
                            'title' => 'Опции объявлений',
                            'ids'   => [
                                'module_id'     => 'admin',
                                'controller_id' => 'boardoption',
                                'action_id'     => '*',
                            ],
                        ],
                        [
                            'href'  => '/admin/boardoptionavailable',
                            'title' => 'Допустимые значения опций объявлений',
                            'ids'   => [
                                'module_id'     => 'admin',
                                'controller_id' => 'boardoptionavailable',
                                'action_id'     => '*',
                            ],
                        ],
                        [
                            'href'  => '/admin/boardoptionincategory',
                            'title' => 'Опции в категориях',
                            'ids'   => [
                                'module_id'     => 'admin',
                                'controller_id' => 'boardoptionincategory',
                                'action_id'     => '*',
                            ],
                        ],
                        [
                            'href'  => '/admin/phone',
                            'title' => 'Телефоны',
                            'ids'   => [
                                'module_id'     => 'admin',
                                'controller_id' => 'phone',
                                'action_id'     => '*',
                            ],
                        ],
                        [
                            'href'  => '/admin/board',
                            'title' => 'Объявления',
                            'ids'   => [
                                'module_id'     => 'admin',
                                'controller_id' => 'board',
                                'action_id'     => '*',
                            ],
                        ],
                    ],
                ],
                [
                    'title' => 'География',
                    'icon'  => '<i class="fas fa-map-marked-alt"></i>',
                    'items' => [
                        [
                            'href'  => '/location/region',
                            'title' => 'Регионы',
                            'ids'   => [
                                'module_id'     => 'location',
                                'controller_id' => 'region',
                                'action_id'     => '*',
                            ],
                        ],
                        [
                            'href'  => '/location/city',
                            'title' => 'Города',
                            'ids'   => [
                                'module_id'     => 'location',
                                'controller_id' => 'city',
                                'action_id'     => '*',
                            ],
                        ],
                        [
                            'href'  => '/location/district',
                            'title' => 'Районы',
                            'ids'   => [
                                'module_id'     => 'location',
                                'controller_id' => 'district',
                                'action_id'     => '*',
                            ],
                        ],
                    ],
                ],

                [
                    'title' => 'Валюта',
                    'icon'  => '<i class="fas fa-euro-sign"></i>',
                    'items' => [
                        [
                            'href'  => '/currency',
                            'title' => 'Валюта',
                            'ids'   => [
                                'module_id'     => 'currency',
                                'controller_id' => '*',
                                'action_id'     => '*',
                            ],
                        ],
                        [
                            'href'  => '/currency/update',
                            'title' => 'Обновить',
                            'ids'   => [
                                'module_id'     => 'currency',
                                'controller_id' => 'update',
                                'action_id'     => '*',
                            ],
                        ],
                    ],
                ],
                [
                    'title' => 'User/RBAC',
                    'icon'  => '<i class="feather icon-home"></i>',
                    'items' => [
                        [
                            'href'  => '/rbac',
                            'title' => 'Назначения',
                            'ids'   => [
                                'module_id'     => 'rbac',
                                'controller_id' => 'assignment',
                                'action_id'     => '*',
                            ],
                        ],
                        [
                            'href'  => '/rbac/route',
                            'title' => 'Пути',
                            'ids'   => [
                                'module_id'     => 'rbac',
                                'controller_id' => 'route',
                                'action_id'     => '*',
                            ],
                        ],
                        [
                            'href'  => '/rbac/permission',
                            'title' => 'Разрешения',
                            'ids'   => [
                                'module_id'     => 'rbac',
                                'controller_id' => 'permission',
                                'action_id'     => '*',
                            ],
                        ],
                        [
                            'href'  => '/rbac/role',
                            'title' => 'Роли',
                            'ids'   => [
                                'module_id'     => 'rbac',
                                'controller_id' => 'role',
                                'action_id'     => '*',
                            ],
                        ],
                        [
                            'href'  => '/user/admin',
                            'title' => 'Пользователи',
                            'ids'   => [
                                'module_id'     => 'user',
                                'controller_id' => 'admin',
                                'action_id'     => '*',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ];

?>





<nav class="pcoded-navbar">
    <div class="pcoded-inner-navbar main-menu">
        <?php foreach ($sidebar[\Yii::getAlias('@apppart')] as $group): ?>
<?php if (isset($group['group_title'])): ?>
            <div class="pcoded-navigatio-lavel"><?=$group['group_title']?></div>
        <?php endif;?>
        <ul class="pcoded-item pcoded-left-item">
            <?php foreach ($group['group_items'] as $group_item): ?>
                <li class="<?php if (isset($group_item['items'])): ?>pcoded-hasmenu<?php endif;?><?php if (isActiveGroup($group_item)): ?> active pcoded-trigger<?php endif;?>">
                    <a href="<?php if (isset($group_item['href'])): ?><?=$group_item['href']?><?php else: ?>javascript:void(0)<?php endif;?>">
                        <span class="pcoded-micon"><?=$group_item['icon']?></span>
                        <span class="pcoded-mtext"><?=$group_item['title']?></span>
                    </a>
                    <?php if (isset($group_item['items'])): ?>
                        <ul class="pcoded-submenu">
                            <?php foreach ($group_item['items'] as $subitem): ?>
                                <li class="<?php if (isActiveSubitem($subitem)): ?>active<?php endif;?>">

                                    <a href="<?=$subitem['href']?>" <?=getSubitemTagOpts($subitem);?>>
                                        <span class="pcoded-mtext"><?=$subitem['title']?></span>
                                    </a>
                                </li>
                            <?php endforeach;?>
                        </ul>
                    <?php endif;?>
<?php ?>
                </li>
            <?php endforeach;?>
        </ul>
        <?php endforeach;?>
    </div>
</nav>
