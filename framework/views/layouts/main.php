<?php
use app\assets\AppAsset;
use app\assets\FontAwesomeAsset;
use yii\helpers\Html;
use yii\bootstrap4\Breadcrumbs;
use xtetis\bootstrap4glyphicons\assets\GlyphiconAsset;

GlyphiconAsset::register($this);
AppAsset::register($this);
FontAwesomeAsset::register($this);


/**
 * Автоподключение яваскриптов
 */
//============================================================
$filename_js_module = '/theme/js/module/'.
    \Yii::getAlias('@module_id').'__'.
    \Yii::getAlias('@controller_id').'.js';
if (file_exists($_SERVER['DOCUMENT_ROOT'].$filename_js_module))
{
    $this->registerJsFile(
        '@web'.$filename_js_module,
        ['depends' => [\yii\web\JqueryAsset::className()]]
    );
}

$filename_js_module = '/theme/js/module/'.
    \Yii::getAlias('@module_id').'__'.
    \Yii::getAlias('@controller_id').'__'.
    \Yii::getAlias('@action_id').'.js';
if (file_exists($_SERVER['DOCUMENT_ROOT'].$filename_js_module))
{
    $this->registerJsFile(
        '@web'.$filename_js_module,
        ['depends' => [\yii\web\JqueryAsset::className()]]
    );
}
//============================================================


$this->registerJsFile(
    '@web/theme/js/common.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);





?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport"
              content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta http-equiv="X-UA-Compatible"
              content="IE=edge">
        <!-- Favicon icon -->
        <link rel="icon"
              href="/theme/templates/adminty/files/assets/images/favicon.ico"
              type="image/x-icon">
        <!-- Google font-->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600"
              rel="stylesheet">
        <!-- zzzzz -->
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <!-- /zzzzz -->
    </head>

    <body>
        <?php $this->beginBody() ?>
        <?=Yii::$app->view->renderFile('@app/views/blocks/preloader.php');?>
        <div id="pcoded"
             class="pcoded">
            <div class="pcoded-overlay-box"></div>
            <div class="pcoded-container navbar-wrapper">
                <?=Yii::$app->view->renderFile('@app/views/blocks/topbar.php');?>
                <div class="pcoded-main-container">
                    <div class="pcoded-wrapper">
                        <?=Yii::$app->view->renderFile('@app/views/blocks/sidebar.php');?>
                        <div class="pcoded-content">
                            <div class="pcoded-inner-content">
                                <div class="main-body">
                                    <div class="page-wrapper">
                                    <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>

                                        <?= $content ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->endBody() ?>
    </body>

</html>
<?php $this->endPage() ?>
