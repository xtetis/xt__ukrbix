<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'language' => 'ru-RU',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
        // Подключаем роуты из модуля "board"
        'board',
    ],
    //'layout' => 'adminlte',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',

        /**
         * Пуьт к сайдбару для пользователей и для админов
         */
        '@apppart'   => 'frontend', // Или backend
        
    ],
    'components' => [
        
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'PgQpRHYwrtXN2rJDJMF0b1r4KZVQIprM',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        /*
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        */
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        
        'authClientCollection' => [
            'class'   => \yii\authclient\Collection::className(),
            'clients' => [
                // here is the list of clients you want to use
                // you can read more in the "Available clients" section
                'facebook' => [
                    'class'        => 'dektrium\user\clients\Facebook',
                    'clientId'     => '358591054865917',
                    'clientSecret' => '58810d2f7e8f078478b2a24bc85d5b56',
                ],
            ],
        ],

        'i18n' => [
            'translations' => [
                'yii2mod.rbac' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@yii2mod/rbac/messages',
                ],
            ],
        ],

        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest', 'user'],
        ],

        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@app/views/user',
                    //'@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-app',
                ],
                
            ],
        ],



        'settings' => [
            'class' => 'pheme\settings\components\Settings'
        ],
    ],
    'params' => $params,






    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableUnconfirmedLogin' => true,
            /**
             * Доступ к админке юзеров только для пользователей с ролью "admin"
             */
            'adminPermission' => 'admin'
        ],
        'rbac' => [
            'class' => 'yii2mod\rbac\Module',
            'as access' => [
                'class' => yii2mod\rbac\filters\AccessControl::class,
            ],
        ],
        'admin' => [
            'class' => 'app\modules\admin\Module',
            'as access' => [
                'class' => yii2mod\rbac\filters\AccessControl::class,
            ],
        ],
        'settings' => [
            'class' => 'pheme\settings\Module',
            'sourceLanguage' => 'ru',
            'as access' => [
                'class' => yii2mod\rbac\filters\AccessControl::class,
            ],
        ],
        /*
        'board' => [
            'class' => 'app\modules\board\Module',
        ],
        */

        'gallery' => [
            'class' => 'xtetis\image\Module',
            'as access' => [
                'class' => yii2mod\rbac\filters\AccessControl::class,
            ],
        ],
        'location' => [
            'class' => 'xtetis\location\Module',
            'as access' => [
                'class' => yii2mod\rbac\filters\AccessControl::class,
            ],
        ],
        'currency' => [
            'class' => 'xtetis\currency\Module',
            'as access' => [
                'class' => yii2mod\rbac\filters\AccessControl::class,
            ],
        ],
        'board' => [
            'class' => 'xtetis\board\Module',
            'as access' => [
                'class' => yii2mod\rbac\filters\AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['default', 'newuser','test', 'index'],
                        'allow' => true,
                        //'roles' => ['?'],
                    ],
                ],
            ],


        ],
    ],

    /**
     * Перед выполнением всех действий - выполняем Behavior
     */
    'as beforeAction' => [
        'class' => 'app\behaviors\AliasesBehavior',
    ],


    
    
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    /**
     * Принудительно очищаем кеш assets
     */
    $config['components']['assetManager']['forceCopy'] = true;
}

return $config;
