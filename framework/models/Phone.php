<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%phone}}".
 *
 * @property int $id Первичный ключ
 * @property int $id_user Ссылка на пользователя
 * @property string $phone Номер телефона в цифрах с кодом страны
 *
 * @property User $user
 */
class Phone extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%phone}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user'], 'required'],
            [['id_user'], 'integer'],
            [['phone'], 'string', 'max' => 20],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Первичный ключ',
            'id_user' => 'Ссылка на пользователя',
            'phone' => 'Номер телефона в цифрах с кодом страны',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user'])->inverseOf('phones');
    }
}
