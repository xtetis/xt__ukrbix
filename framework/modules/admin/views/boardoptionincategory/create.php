<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BoardOptionInCategory */

$this->title = Yii::t('app', 'Create Board Option In Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Board Option In Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="board-option-in-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
