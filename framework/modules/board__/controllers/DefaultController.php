<?php

namespace app\modules\board\controllers;

use Yii;
use yii\web\Controller;
use app\models\Board;
use app\models\BoardSearch;


/**
 * Default controller for the `board` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * http://ukrbix.lc/board/2/r-2
     * http://ukrbix.lc/board/2/2
     * http://ukrbix.lc/board/2
     * 
     * @return string
     */
    public function actionIndex(
        $id_category = 0,
        $type_region = '',
        $id_region_city = 0
    )
    {
        /*
        echo 123123;
        $numargs = func_get_args();
        print_r($numargs);
        */
        //return $this->render('index');

        $searchModel = new BoardSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionAll()
    {
        echo 456456;
    }
}
