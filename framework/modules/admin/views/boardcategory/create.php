<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BoardCategory */

$this->title = Yii::t('app', 'Create Board Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Board Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="board-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
