<?php

use yii\db\Migration;

/**
 * Class m190412_184301_auth_data
 */
class m190412_184301_auth_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $auth_item = array(
            array('name' => '/admin/*','type' => '2','description' => NULL,'rule_name' => NULL,'data' => NULL,'created_at' => '1554105521','updated_at' => '1554105521'),
            array('name' => '/rbac/*','type' => '2','description' => NULL,'rule_name' => NULL,'data' => NULL,'created_at' => '1554063314','updated_at' => '1554063314'),
            array('name' => '/settings/*','type' => '2','description' => NULL,'rule_name' => NULL,'data' => NULL,'created_at' => '1554186043','updated_at' => '1554186043'),
            array('name' => '/user/admin/*','type' => '2','description' => NULL,'rule_name' => NULL,'data' => NULL,'created_at' => '1554064167','updated_at' => '1554064167'),
            array('name' => 'admin','type' => '1','description' => 'Администратор сайта','rule_name' => NULL,'data' => NULL,'created_at' => '1554061477','updated_at' => '1554061477')
          );

          foreach ($auth_item as $v) {
            $this->insert('auth_item', $v);
        }


        $auth_item_child = array(
            array('parent' => 'admin','child' => '/admin/*'),
            array('parent' => 'admin','child' => '/rbac/*'),
            array('parent' => 'admin','child' => '/settings/*'),
            array('parent' => 'admin','child' => '/user/admin/*')
          );
          foreach ($auth_item_child as $v) {
            $this->insert('auth_item_child', $v);
        }

        $auth_assignment = array(
            array('item_name' => 'admin','user_id' => '2','created_at' => '1554061879')
          );

        foreach ($auth_assignment as $v) {
            $this->insert('auth_assignment', $v);
        }

          
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('auth_assignment', []);
        $this->delete('auth_item', []);
        $this->delete('auth_item_child', []);

    }


    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190412_184301_auth_data cannot be reverted.\n";

        return false;
    }
    */
}
