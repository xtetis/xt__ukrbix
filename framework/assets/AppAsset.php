<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $basePath = '@webroot';
    /**
     * @var string
     */
    public $baseUrl = '@web';

    /**
     * @var array
     */
    public $css = [
        'theme/templates/adminty/files/assets/icon/themify-icons/themify-icons.css',
        'theme/templates/adminty/files/assets/icon/icofont/css/icofont.css',

        'theme/templates/adminty/files/assets/icon/feather/css/feather.css',
        'theme/templates/adminty/files/assets/css/style.css',
        'theme/templates/adminty/files/assets/css/jquery.mCustomScrollbar.css',
        
        'theme/css/common.css',
    ];
    /**
     * @var array
     */
    public $js = [
        'theme/templates/adminty/files/bower_components/jquery-ui/js/jquery-ui.min.js',
        'theme/templates/adminty/files/bower_components/popper.js/js/popper.min.js',
        'theme/templates/adminty/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js',
        'theme/templates/adminty/files/bower_components/modernizr/js/modernizr.js',
        'theme/templates/adminty/files/bower_components/chart.js/js/Chart.js',

        'theme/templates/adminty/files/assets/js/vartical-layout.min.js',
        'theme/templates/adminty/files/assets/pages/dashboard/custom-dashboard.js',
        'theme/templates/adminty/files/assets/js/script.js',

        'theme/templates/adminty/files/assets/pages/widget/amchart/amcharts.js',
        'theme/templates/adminty/files/assets/pages/widget/amchart/serial.js',
        'theme/templates/adminty/files/assets/pages/widget/amchart/light.js',

        'theme/templates/adminty/files/assets/js/jquery.mCustomScrollbar.concat.min.js',

        'theme/templates/adminty/files/assets/js/SmoothScroll.js',
        'theme/templates/adminty/files/assets/js/pcoded.min.js',
    ];
    /**
     * @var array
     */
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
        'yii\bootstrap4\BootstrapPluginAsset',
    ];
}
