<?php

namespace app\models;

use Yii;
use app\models\City;
use app\models\Region;
use app\models\Currency;
use app\models\District;

/**
 * This is the model class for table "{{%board}}".
 *
 * @property int $id Первичный ключ
 * @property int $id_user Ссылка на пользователя
 * @property int $id_district Ссылка на район
 * @property int $id_city Ссылка на город
 * @property int $id_board_category Ссылка категорию объявления
 * @property int $id_currency Ссылка валюту
 * @property string $name Заголовок объявления
 * @property string $about Текст объявления
 * @property int $price Цена
 * @property string $create_date Дата создания
 * @property string $pub_date Дата публикации
 * @property string $source Источник объявления
 * @property int $active Активность объявления
 * @property int $id_album ID альбома
 *
 *
 * @property BoardCategory $boardCategory
 * @property City $city
 * @property Currency $currency
 * @property Region $region
 * @property User $user
 * @property BoardAbuse[] $boardAbuses
 */
class Board extends \yii\db\ActiveRecord
{

    public $email;
    public $username;


/*
    function behaviors()
    {
        return [
            'images' => [
                'class' => 'dvizh\gallery\behaviors\AttachImages',
                'mode' => 'gallery',
                'quality' => 60,
                'galleryId' => 'picture'
            ],
        ];
    }
*/

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%board}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'name', 'about', 'id_city', 'id_board_category', 'id_currency'], 'required'],
            [['id_user', 'id_district', 'id_album', 'id_city', 'id_board_category', 'id_currency', 'price', 'active'], 'integer'],

            ['about', 'string', 'min' => 50],
            ['name', 'string', 'min' => 10],

            [['create_date', 'pub_date'], 'safe'],
            [['name', 'source'], 'string', 'max' => 200],
            [['id_board_category'], 'exist', 'skipOnError' => true, 'targetClass' => BoardCategory::className(), 'targetAttribute' => ['id_board_category' => 'id']],
            [['id_city'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['id_city' => 'id']],
            [['id_currency'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['id_currency' => 'id']],
            //[['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],

            [['id_user', 'id_district', 'id_album', 'id_city', 'id_board_category', 'id_currency'], 'filter', 'filter' => 'intval'],

            ['id_district', 'validateIdDistrict'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'Первичный ключ',
            'id_user'           => 'Ссылка на пользователя',
            'id_district'       => 'Район',
            'id_city'           => 'Город',
            'id_board_category' => 'Ссылка категорию объявления',
            'id_currency'       => 'Валюта',
            'name'              => 'Заголовок',
            'about'             => 'Текст объявления',
            'price'             => 'Цена',
            'create_date'       => 'Дата создания',
            'pub_date'          => 'Дата публикации',
            'source'            => 'Источник объявления',
            'active'            => 'Активность объявления',
            'id_album'          => 'Альбом',
        ];
    }

    /**
     * @param $attribute_name
     * @param $params
     */
    public function validateIdDistrict(
        $attribute_name,
        $params
    )
    {
        if ($this->id_district)
        {
            $count = District::find()->
                where(
                [
                    'id_city' => intval($this->id_city),
                    'id'      => intval($this->id_district),
                ])->
                count();

            if (!$count)
            {
                $this->addError($attribute_name, 'Выбранный район не соответствует городу');

                return false;
            }
        }

        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoardCategory()
    {
        return $this->hasOne(BoardCategory::className(), ['id' => 'id_board_category'])->inverseOf('boards');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'id_city'])->inverseOf('boards');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'id_currency'])->inverseOf('boards');
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user'])->inverseOf('boards');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoardAbuses()
    {
        return $this->hasMany(BoardAbuse::className(), ['id_board' => 'id'])->inverseOf('board');
    }
}
