<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%board_abuse}}".
 *
 * @property int $id Первичный ключ
 * @property int $id_board Ссылка на объявление
 * @property int $id_user Ссылка на пользователя
 * @property string $message Текст жалобы
 * @property string $create_date Дата создания
 *
 * @property Board $board
 * @property User $user
 */
class BoardAbuse extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%board_abuse}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_board', 'id_user'], 'required'],
            [['id_board', 'id_user'], 'integer'],
            [['create_date'], 'safe'],
            [['message'], 'string', 'max' => 200],
            [['id_board'], 'exist', 'skipOnError' => true, 'targetClass' => Board::className(), 'targetAttribute' => ['id_board' => 'id']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Первичный ключ',
            'id_board' => 'Ссылка на объявление',
            'id_user' => 'Ссылка на пользователя',
            'message' => 'Текст жалобы',
            'create_date' => 'Дата создания',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoard()
    {
        return $this->hasOne(Board::className(), ['id' => 'id_board'])->inverseOf('boardAbuses');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user'])->inverseOf('boardAbuses');
    }
}
