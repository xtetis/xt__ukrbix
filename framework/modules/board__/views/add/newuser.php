<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;


    $this->title                   = Yii::t('app', 'Ваше объявление добавлено');
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Объявления'), 'url' => ['/board']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="board-create">

    <h1><?=Html::encode($this->title)?></h1>

    <div class="board-newuser">




        <div class="alert alert-primary  text-center">
            <a class="btn btn-primary"
               target="_blank"
               href="/board/item/<?=$board_item?>">Перейти к объявлению</a>
        </div>

        <div>При добавлении объявлении создан пользователь со слудующими данными:</div>
        <table class="table">
            <tbody>
                <tr>
                    <th>Логин</th>
                    <td><?=$user['username']?></td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td><?=$user['email']?></td>
                </tr>
                <tr>
                    <th>Пароль</th>
                    <td><?=$user['password']?></td>
                </tr>
            </tbody>
        </table>
        <div class="">Регистрационные данные отправлены Вам на электронную почту.
            Сменить пароль Вы можете на странице
            <a href="/user/settings/account" class="text-primary"
               target="_blank">Настройки аккаунта
            </a>
        </div>
    </div>
</div>
