<?php

namespace app\modules\board\models;

use Yii;
use yii\base\Model;


/**
 * ContactForm is the model behind the contact form.
 */
class FormCategory extends Model
{
    
    public $id_category1;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['id_category1'], 'required'],
            
        ];
    }


    public function attributeLabels()
    {
        return [
            'id_category1' => 'xxxxxx',
        ];
    }
}
