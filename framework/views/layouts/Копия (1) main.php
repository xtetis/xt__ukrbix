<?php

/* @var $this \yii\web\View */
/* @var $content string */

//use app\widgets\Alert;



use yii\helpers\Html;
//use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;

use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\assets\FontAwesomeAsset;


FontAwesomeAsset::register($this);
AppAsset::register($this);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible"
              content="IE=edge">
        <meta name="viewport"
              content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>

    </head>

    <body id="page-top">
    <?php $this->beginBody() ?>
        <!-- Page Wrapper -->
        <div id="wrapper">

            <?=Yii::$app->view->renderFile('@sidebar_path');?>

            <!-- Content Wrapper -->
            <div id="content-wrapper"
                 class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content">

                    <?=Yii::$app->view->renderFile('@app/views/blocks/topbar.php');?>

                    <!-- Begin Page Content -->
                    <div class="container-fluid">

                        <div>
                            <?= $content ?>
                        </div>

                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright &copy; Your Website 2019</span>
                        </div>
                    </div>
                </footer>
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded"
           href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>


        <?php $this->endBody() ?>
    </body>

</html>
<?php $this->endPage() ?>
