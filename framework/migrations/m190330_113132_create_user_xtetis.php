<?php

use yii\db\Migration;

/**
 * Class m190330_113132_create_user_xtetis
 */
class m190330_113132_create_user_xtetis extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $user = array(
            array('id' => '2','username' => 'xtetis','email' => 'tihonenkovaleriy@gmail.com','password_hash' => '$2y$10$36mynhKwHfrvvUPVltDkE.4CtNM7KgZ2hZzm1zywhvV/wMD/H83jK','auth_key' => 'AJ9aUe1j4H6-dqh09RnkZgKr61Wf-DnT','confirmed_at' => '1553712869','unconfirmed_email' => NULL,'blocked_at' => NULL,'registration_ip' => '127.0.0.1','created_at' => '1553712490','updated_at' => '1553712490','flags' => '0','last_login_at' => '1553944350')
          );

        foreach ($user as $v) {
            $this->insert('user', $v);
        }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('user', ['id' => 2]);
    }

}
