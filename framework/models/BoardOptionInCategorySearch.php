<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BoardOptionInCategory;

/**
 * BoardOptionInCategorySearch represents the model behind the search form of `app\models\BoardOptionInCategory`.
 */
class BoardOptionInCategorySearch extends BoardOptionInCategory
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_board_option', 'id_board_category', 'use_setting', 'use_filter'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BoardOptionInCategory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_board_option' => $this->id_board_option,
            'id_board_category' => $this->id_board_category,
            'use_setting' => $this->use_setting,
            'use_filter' => $this->use_filter,
        ]);

        return $dataProvider;
    }
}
