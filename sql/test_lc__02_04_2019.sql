-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Апр 02 2019 г., 20:39
-- Версия сервера: 10.1.37-MariaDB
-- Версия PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `test.lc`
--

-- --------------------------------------------------------

--
-- Структура таблицы `ad_category`
--

DROP TABLE IF EXISTS `ad_category`;
CREATE TABLE `ad_category` (
  `id` int(11) NOT NULL COMMENT 'Первичный ключ',
  `id_parent` int(11) DEFAULT '0' COMMENT 'Родительская категория',
  `name` varchar(200) DEFAULT NULL COMMENT 'Название категории',
  `hone` varchar(200) DEFAULT NULL COMMENT 'H1 страницы',
  `title` varchar(200) DEFAULT NULL COMMENT 'Title страницы',
  `description` varchar(200) DEFAULT NULL COMMENT 'Description страницы',
  `active` int(11) DEFAULT '1' COMMENT 'Активность записи'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Список категорий объявлений';

--
-- Дамп данных таблицы `ad_category`
--

INSERT INTO `ad_category` VALUES(1, NULL, 'ww', '', '', '', 1);
INSERT INTO `ad_category` VALUES(2, NULL, '222', '', '', '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_assignment`
--

INSERT INTO `auth_assignment` VALUES('admin', '2', 1554061879);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_item`
--

INSERT INTO `auth_item` VALUES('/admin/*', 2, NULL, NULL, NULL, 1554105521, 1554105521);
INSERT INTO `auth_item` VALUES('/rbac/*', 2, NULL, NULL, NULL, 1554063314, 1554063314);
INSERT INTO `auth_item` VALUES('/settings/*', 2, NULL, NULL, NULL, 1554186043, 1554186043);
INSERT INTO `auth_item` VALUES('/user/admin/*', 2, NULL, NULL, NULL, 1554064167, 1554064167);
INSERT INTO `auth_item` VALUES('admin', 1, 'Администратор сайта', NULL, NULL, 1554061477, 1554061477);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_item_child`
--

INSERT INTO `auth_item_child` VALUES('admin', '/admin/*');
INSERT INTO `auth_item_child` VALUES('admin', '/rbac/*');
INSERT INTO `auth_item_child` VALUES('admin', '/settings/*');
INSERT INTO `auth_item_child` VALUES('admin', '/user/admin/*');

-- --------------------------------------------------------

--
-- Структура таблицы `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `city`
--

DROP TABLE IF EXISTS `city`;
CREATE TABLE `city` (
  `id` int(11) NOT NULL COMMENT 'Первичный ключ',
  `id_region` int(11) NOT NULL COMMENT 'Регион. Ссылка на таблицу region',
  `name` varchar(200) DEFAULT NULL COMMENT 'Название города',
  `active` int(11) DEFAULT '1' COMMENT 'Активность записи'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Список городов';

--
-- Дамп данных таблицы `city`
--

INSERT INTO `city` VALUES(1, 1, 'Кривой Рог', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `district`
--

DROP TABLE IF EXISTS `district`;
CREATE TABLE `district` (
  `id` int(11) NOT NULL COMMENT 'Первичный ключ',
  `id_city` int(11) NOT NULL COMMENT 'Город. Ссылка на таблицу city',
  `name` varchar(200) DEFAULT NULL COMMENT 'Название района',
  `active` int(11) DEFAULT '1' COMMENT 'Активность записи'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Список районов города';

--
-- Дамп данных таблицы `district`
--

INSERT INTO `district` VALUES(1, 1, 'Металлургический', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` VALUES('m000000_000000_base', 1554060080);
INSERT INTO `migration` VALUES('m140209_132017_init', 1554060087);
INSERT INTO `migration` VALUES('m140403_174025_create_account_table', 1554060090);
INSERT INTO `migration` VALUES('m140504_113157_update_tables', 1554060095);
INSERT INTO `migration` VALUES('m140504_130429_create_token_table', 1554060097);
INSERT INTO `migration` VALUES('m140506_102106_rbac_init', 1554060416);
INSERT INTO `migration` VALUES('m140618_045255_create_settings', 1554186582);
INSERT INTO `migration` VALUES('m140830_171933_fix_ip_field', 1554060098);
INSERT INTO `migration` VALUES('m140830_172703_change_account_table_name', 1554060098);
INSERT INTO `migration` VALUES('m141222_110026_update_ip_field', 1554060099);
INSERT INTO `migration` VALUES('m141222_135246_alter_username_length', 1554060100);
INSERT INTO `migration` VALUES('m150227_114524_init', 1554060112);
INSERT INTO `migration` VALUES('m150614_103145_update_social_account_table', 1554060104);
INSERT INTO `migration` VALUES('m150623_212711_fix_username_notnull', 1554060104);
INSERT INTO `migration` VALUES('m151126_091910_add_unique_index', 1554186582);
INSERT INTO `migration` VALUES('m151218_234654_add_timezone_to_profile', 1554060104);
INSERT INTO `migration` VALUES('m160929_103127_add_last_login_at_to_user_table', 1554060106);
INSERT INTO `migration` VALUES('m161109_104201_rename_setting_table', 1554060112);
INSERT INTO `migration` VALUES('m170323_102933_add_description_column_to_setting_table', 1554060113);
INSERT INTO `migration` VALUES('m170413_125133_rename_date_columns', 1554060113);
INSERT INTO `migration` VALUES('m170907_052038_rbac_add_index_on_auth_assignment_user_id', 1554060417);
INSERT INTO `migration` VALUES('m180523_151638_rbac_updates_indexes_without_prefix', 1554060418);
INSERT INTO `migration` VALUES('m190329_185057_create_region_table', 1554060438);
INSERT INTO `migration` VALUES('m190329_190347_create_city_table', 1554060441);
INSERT INTO `migration` VALUES('m190329_193504_create_district_table', 1554060443);
INSERT INTO `migration` VALUES('m190330_113132_create_user_xtetis', 1554060443);
INSERT INTO `migration` VALUES('m190402_175518_create_ad_category_table', 1554229031);

-- --------------------------------------------------------

--
-- Структура таблицы `profile`
--

DROP TABLE IF EXISTS `profile`;
CREATE TABLE `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci,
  `timezone` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `profile`
--

INSERT INTO `profile` VALUES(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `region`
--

DROP TABLE IF EXISTS `region`;
CREATE TABLE `region` (
  `id` int(11) NOT NULL COMMENT 'Первичный ключ',
  `name` varchar(200) DEFAULT NULL COMMENT 'Имя региона',
  `active` int(11) DEFAULT '1' COMMENT 'Активность записи'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Список регионов';

--
-- Дамп данных таблицы `region`
--

INSERT INTO `region` VALUES(1, 'Днепропетровская', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `section` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` text,
  `active` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `social_account`
--

DROP TABLE IF EXISTS `social_account`;
CREATE TABLE `social_account` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `token`
--

DROP TABLE IF EXISTS `token`;
CREATE TABLE `token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `last_login_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` VALUES(2, 'xtetis', 'tihonenkovaleriy@gmail.com', '$2y$10$36mynhKwHfrvvUPVltDkE.4CtNM7KgZ2hZzm1zywhvV/wMD/H83jK', 'AJ9aUe1j4H6-dqh09RnkZgKr61Wf-DnT', 1553712869, NULL, NULL, '127.0.0.1', 1553712490, 1553712490, 0, 1554060961);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `ad_category`
--
ALTER TABLE `ad_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-id_parent` (`id_parent`);

--
-- Индексы таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `idx-auth_assignment-user_id` (`user_id`);

--
-- Индексы таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Индексы таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Индексы таблицы `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Индексы таблицы `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-city-id_region` (`id_region`);

--
-- Индексы таблицы `district`
--
ALTER TABLE `district`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-district-id_city` (`id_city`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`user_id`);

--
-- Индексы таблицы `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_unique_key_section` (`section`,`key`);

--
-- Индексы таблицы `social_account`
--
ALTER TABLE `social_account`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `account_unique` (`provider`,`client_id`),
  ADD UNIQUE KEY `account_unique_code` (`code`),
  ADD KEY `fk_user_account` (`user_id`);

--
-- Индексы таблицы `token`
--
ALTER TABLE `token`
  ADD UNIQUE KEY `token_unique` (`user_id`,`code`,`type`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_unique_username` (`username`),
  ADD UNIQUE KEY `user_unique_email` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `ad_category`
--
ALTER TABLE `ad_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Первичный ключ', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Первичный ключ', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `district`
--
ALTER TABLE `district`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Первичный ключ', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `region`
--
ALTER TABLE `region`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Первичный ключ', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `social_account`
--
ALTER TABLE `social_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `city`
--
ALTER TABLE `city`
  ADD CONSTRAINT `fk-city-id_region` FOREIGN KEY (`id_region`) REFERENCES `region` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `district`
--
ALTER TABLE `district`
  ADD CONSTRAINT `fk-district-id_city` FOREIGN KEY (`id_city`) REFERENCES `city` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `fk_user_profile` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `social_account`
--
ALTER TABLE `social_account`
  ADD CONSTRAINT `fk_user_account` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `token`
--
ALTER TABLE `token`
  ADD CONSTRAINT `fk_user_token` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
