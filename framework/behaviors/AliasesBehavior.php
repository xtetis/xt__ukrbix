<?php
namespace app\behaviors;

use Yii;
use yii\base\Behavior;
use yii\base\Controller;

/**
 * Class AliasesBehavior.
 */
class AliasesBehavior extends Behavior
{

    /**
     * @var array
     */
    public $sidebar_admin_enable = [
        'modules' => [
            'rbac' => ['*'],
            'settings' => ['*'],
            'admin' => ['*'],
            'location' => ['*'],
            'currency' => ['*'],
            'user' => ['admin'],
        ],
    ];

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            Controller::EVENT_BEFORE_ACTION => 'beforeAction',
        ];
    }
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {

        /**
         * Устанавливаем путь к адмнискому сайдбару для некоторых модулей
         */
        if (isset($this->sidebar_admin_enable['modules'][$action->action->controller->module->id]))
        {
            $module_controllers = $this->sidebar_admin_enable['modules'][$action->action->controller->module->id];
            if ((in_array('*', $module_controllers)) || (in_array($action->action->controller->id, $module_controllers)))
            {
                \Yii::setAlias('@apppart', 'backend');
            }
        }



        \Yii::setAlias('@module_id', $action->action->controller->module->id);
        \Yii::setAlias('@controller_id', $action->action->controller->id);
        \Yii::setAlias('@action_id', $action->action->id);

        return true;
    }
}
