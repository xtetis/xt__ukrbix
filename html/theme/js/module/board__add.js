$( document ).ready(function() {

    $('#accordion').collapse();

    $('#accordion_region').collapse();


    if (!$('#ad-id_region').val())
    {
        $('.field-boardaddform-id_city').hide();
    }
    
    if (!$('#ad-id_city').val())
    {
        $('.field-boardaddform-id_district').hide();
    }

    /**
     * Изменение региона
     */
    //=====================================================================
    $(document).on('change', '#board-id_region', function() {
        var id_region = $(this).val();
        $('#btn_refresh_id_city_block').attr('href','/board/add?id_region='+id_region);
        $("#btn_refresh_id_city_block").click();
    });
    //=====================================================================

    /**
     * Изменение города
     */
    //=====================================================================
    $(document).on('change', '#board-id_city', function() {
        var id_city = $(this).val();
        $('#btn_refresh_id_district_block').attr('href','/board/add?id_city='+id_city);
        $("#btn_refresh_id_district_block").click();
    });
    //=====================================================================


    /**
     * Клик по категории
     */
    //=====================================================================
    $(document).on('click', '#modal__select_id_board_category .bc_level', function() {
        var idx = $(this).attr('idx');
        var level = Number($(this).attr('level'));
        var has_child = $(this).attr('has_child');
        var text = $(this).text().trim();
        var parent = Number($(this).attr('parent'));
        

        if (has_child == 0)
        {
            var text_parent = $('#modal__select_id_board_category .bc_level.bc_id_'+parent).text().trim();
            $('#board-id_board_category').val(idx);
            $('#name_id_board_category').val(text_parent+' -> '+text);
            $('#modal__select_id_board_category').modal('hide');
            return true;
        }
        
        console.log(level);
        console.log(idx);
        $('#modal__select_id_board_category .card_level_'+(level+1)+' #heading'+(level+1)+' button').text(text);
        $('#modal__select_id_board_category .modal-body .card_level_'+(level+1)).show();
        $('#modal__select_id_board_category .modal-body .card_level_'+(level+2)).hide();
        $('#modal__select_id_board_category .modal-body .card_level_'+(level+1)+' #collapse'+(level+1)).addClass('show');
        $('#modal__select_id_board_category .modal-body .card_level_'+(level)+' #collapse'+(level)).removeClass('show');
        

        $('#modal__select_id_board_category .bc_level_'+(level+1)).hide();
        $('#modal__select_id_board_category .bc_level_'+(level+2)).hide();
        $('#modal__select_id_board_category .bc_parent_'+idx).show();
        console.log('#modal__select_id_board_category .modal-body .card_level_'+(level+1));
    });
    //=====================================================================






    /**
     * Клик по области/городу
     */
    //=====================================================================
    $(document).on('click', '#modal__select_id_city .region_item', function() {
        var idx = $(this).attr('idx');
        var text = $(this).text().trim();

        $('#modal__select_id_city #heading_city button').text(text);
        $('#modal__select_id_city .modal-body .city_item').hide();
        $('#modal__select_id_city .modal-body .city_item_parent_'+idx).show();

        $('#modal__select_id_city .modal-body #collapse_region').removeClass('show');
        $('#modal__select_id_city .modal-body #collapse_city').addClass('show');

        $('#modal__select_id_city .modal-body .card.card_city').show();
    });

    $(document).on('click', '#modal__select_id_city .city_item', function() {
        var idx = $(this).attr('idx');
        var text = $(this).text().trim();
        var parent = Number($(this).attr('parent'));
        var text_parent = $('#modal__select_id_city .region_item.region_item_'+parent).text().trim();

        $('#board-id_city').val(idx);
        $('#name_id_city').val(text_parent+' -> '+text);
        $('#modal__select_id_city').modal('hide');

        $('#btn_refresh_id_district_block').attr('href','/board/add?id_city='+idx);
        $("#btn_refresh_id_district_block").click();
    });
    //=====================================================================



});