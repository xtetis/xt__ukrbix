<?php

namespace app\modules\board\models;

use Yii;
use yii\base\Model;
use app\models\Region;
use app\models\City;
use app\models\District;
use app\models\Currency;


/**
 * ContactForm is the model behind the contact form.
 */
class BoardAddForm extends Model
{
    
    public $id;
    public $id_region;
    public $id_city;
    public $id_district;
    public $name;
    public $email;
    public $about;
    public $price;
    public $id_currency;
    /*
    public $email;
    public $subject;
    public $body;
    public $verifyCode;
    */



    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['id_region','id_city','name','about','id_currency'], 'required'],
            [['id_district', 'id_city', 'id_region', 'price','id_currency'], 'integer'],
            [['name'], 'string', 'max' => 200],
            [['about'], 'string', 'min' => 50],
            [['price'], 'integer', 'min' => 0],
            
            // email has to be a valid email address
            //['email', 'email'],
            // verifyCode needs to be entered correctly
            //['verifyCode', 'captcha'],
            [['id_region'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['id_region' => 'id']],
            [['id_city'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['id_city' => 'id']],
            [['id_district'], 'exist', 'skipOnError' => true, 'targetClass' => District::className(), 'targetAttribute' => ['id_district' => 'id']],
            [['id_currency'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['id_currency' => 'id']],
            
            ['id_city', 'validateIdCity'],
            ['id_district', 'validateIdDistrict'],
        ];
    }


    public function validateIdCity(
        $attribute_name,
        $params
    )
    {
        $count = City::find()->
            where(
                [
                    'id_region' => intval($this->id_region),
                    'id' => intval($this->id_city),
                ])->
            count();

        if (!$count)
        {
            $this->addError($attribute_name, 'Выбранный город не соответствует области');

            return false;
        }

        return true;
    }


    public function validateIdDistrict(
        $attribute_name,
        $params
    )
    {
        $count = District::find()->
            where(
                [
                    'id_city' => intval($this->id_city),
                    'id' => intval($this->id_district),
                ])->
            count();

        if (!$count)
        {
            $this->addError($attribute_name, 'Выбранный район не соответствует городу');

            return false;
        }

        return true;
    }



    public function attributeLabels()
    {
        return [
            'id_region' => 'Область',
            'id_city' => 'Город',
            'name' => 'Заголовок',
            'id_district' => 'Район',
            'about' => 'Текст объявления',
            'price' => 'Цена',
            'id_currency' => 'Валюта',
        ];
    }
}
