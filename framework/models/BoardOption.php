<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%board_option}}".
 *
 * @property int $id Первичный ключ
 * @property int $option_type Тип элемента ввода (0 - select, 1 - text input)
 * @property string $name Название категории
 * @property string $description Краткое описание опции
 * @property string $introtext Подробное описание опции
 * @property int $filter_type ХЗ
 * @property int $active Активность записи
 *
 * @property BoardOptionAvailable[] $boardOptionAvailables
 * @property BoardOptionInCategory[] $boardOptionInCategories
 */
class BoardOption extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%board_option}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['option_type', 'filter_type', 'active'], 'integer'],
            [['name', 'description'], 'string', 'max' => 200],
            [['introtext'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Первичный ключ',
            'option_type' => 'Тип элемента ввода (0 - select, 1 - text input)',
            'name' => 'Название категории',
            'description' => 'Краткое описание опции',
            'introtext' => 'Подробное описание опции',
            'filter_type' => 'ХЗ',
            'active' => 'Активность записи',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoardOptionAvailables()
    {
        return $this->hasMany(BoardOptionAvailable::className(), ['id_board_option' => 'id'])->inverseOf('boardOption');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoardOptionInCategories()
    {
        return $this->hasMany(BoardOptionInCategory::className(), ['id_board_option' => 'id'])->inverseOf('boardOption');
    }
}
