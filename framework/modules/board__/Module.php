<?php

namespace app\modules\board;

use Yii;
use yii\base\BootstrapInterface;

/**
 * board module definition class
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\board\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // initialize the module with the configuration loaded from config.php
        Yii::configure($this, require(__DIR__ . '/config/main.php'));
    }



    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules([
            /*
            [
                'class' => 'yii\web\UrlRule', 
                'pattern' => $this->id, 
                'route' => $this->id . '/tools/index'
            ],
            [
                'class' => 'yii\web\UrlRule', 
                'pattern' => $this->id . '/<controller:[\w\-]+>/<action:[\w\-]+>', 
                'route' => $this->id . '/<controller>/<action>'
            ],
            */
            [
                'class' => 'yii\web\UrlRule', 
                'pattern' => $this->id . '/item/<id:\d+>', 
                'route' => $this->id . '/item/index'
            ],
            [
                'class' => 'yii\web\UrlRule', 
                'pattern' => $this->id . '', 
                'route' => $this->id . '/default/index'
            ],
            [
                'class' => 'yii\web\UrlRule', 
                'pattern' => $this->id . '/<id_category:\d+>', 
                'route' => $this->id . '/default/index'
            ],
            [
                'class' => 'yii\web\UrlRule', 
                'pattern' => $this->id . '/<id_category:\d+>/<type_region:(r|)><id_region_city:\d+>', 
                'route' => $this->id . '/default/index'
            ],
        ], false);
    }
}
